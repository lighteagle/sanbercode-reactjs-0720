// No. 1 
console.log("------------------------")
console.log("Soal No. 1")
console.log("------------------------")

var loopingPertama = "I love coding"
var loopingKedua = "I will become a frontend developer"

var count = 0

console.log("LOOPING PERTAMA")
while (count < 20) {
    count += 2
    console.log(`${count} - ${loopingPertama}`)
}

console.log("LOOPING KEDUA")
while (count > 0) {
    console.log(`${count} - ${loopingKedua}`)
    count -= 2
}


// No. 2
console.log("\n----------------------------------")
console.log("Soal No. 2 ")
console.log("----------------------------------")

var ganjil = 'Santai'
var genap = 'Berkualitas'
var x3_ganjil = ' I Love Coding'

for (let i = 1; i <= 20; i++) {
    if (i % 3 === 0 && i % 2 === 1) {
        console.log(`${i} - ${x3_ganjil}`);
    } else if (i % 2 === 1) {
        console.log(`${i} - ${ganjil}`);
    } else {
        console.log(`${i} - ${genap}`);
    }
}


// No. 3
console.log("\n----------------------------------")
console.log("Soal No. 3")
console.log("----------------------------------")
// Buat Segitiga dengan dimensi : tinggi 7 x alas 7
var pagar = "#"
var segitiga = ''
var tinggi = 7
var alas = 7

for (let i = 1; i <= tinggi; i++) {
    for (let j = alas - i; j < alas; j++) {
        segitiga += pagar
    }
    segitiga += "\n"
}

console.log(segitiga);

// No. 4
console.log("\n----------------------------------")
console.log("Soal No. 4")
console.log("----------------------------------")

var kalimat = "saya sangat senang belajar javascript"
var arrKalimat = kalimat.split(" ")

console.log(arrKalimat)

// No. 5
console.log("\n----------------------------------")
console.log("Soal No. 5")
console.log("----------------------------------")

var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"]

daftarBuah.sort().map((buah) => {
    console.log(buah)
})