// soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

var output = kataPertama
output += " " + kataKedua[0].toUpperCase() + kataKedua.slice(1)
output += " " + kataKetiga
output += " " + kataKeempat.toUpperCase()

console.log("Jawaban Soal 1 => ", output)

// soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

var output = parseInt(kataPertama)
output += parseInt(kataKedua)
output += parseInt(kataKetiga)
output += parseInt(kataKeempat)

console.log("Jawaban Soal 2 => ", output)

// soal 3
var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14); // do your own! 
var kataKetiga = kalimat.substring(15, 18); // do your own! 
var kataKeempat = kalimat.substring(19, 24); // do your own! 
var kataKelima = kalimat.substring(25, 31); // do your own! 

console.log("Jawaban Soal 3")
console.log("--------------")
console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);

// soal 4
var nilai = 0
var index = ""

// hardcode untuk input nilai
nilai = 75

if (nilai >= 80) {
    index = "A"
} else if (nilai >= 70) {
    index = "B"
} else if (nilai >= 60) {
    index = "C"
} else if (nilai >= 50) {
    index = "D"
} else {
    index = "E"
}

console.log("Jawaban Soal 4 => ", `Input Nilai: ${nilai} maka Index : ${index}`)

// Soal 5
var tanggal = 18;
var bulan = 11;
var tahun = 1982;
var bulan_text = "";

switch (bulan) {
    case 1:
        bulan_text = 'Januari';
        break;
    case 2:
        bulan_text = 'Februari'
        break;
    case 3:
        bulan_text = 'Maret'
        break;
    case 4:
        bulan_text = 'April'
        break;
    case 5:
        bulan_text = 'Mei'
        break;
    case 6:
        bulan_text = 'Juni'
        break;
    case 7:
        bulan_text = 'Juli'
        break;
    case 8:
        bulan_text = 'Agustus'
        break;
    case 9:
        bulan_text = 'September'
        break;
    case 10:
        bulan_text = 'Oktober'
        break;
    case 11:
        bulan_text = 'November'
        break;
    case 12:
        bulan_text = 'Desember'
        break;
}

var output = tanggal
output += " " + bulan_text
output += " " + tahun

console.log("Jawaban Soal 5 => ", output)