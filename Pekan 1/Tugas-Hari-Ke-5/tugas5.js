// No. 1 
console.log("------------------------")
console.log("Soal No. 1")
console.log("------------------------")

function halo() {
    return "Halo Sanbers!"
}

console.log(halo()) // "Halo Sanbers!" 

// No. 2 
console.log("\n------------------------")
console.log("Soal No. 2")
console.log("------------------------")

function kalikan(num1, num2) {
    return num1 * num2
}

var num1 = 12
var num2 = 4

var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48



// No. 3 
console.log("\n------------------------")
console.log("Soal No. 3")
console.log("------------------------")

function introduce(name, age, address, hobby) {
    var intro = "Nama saya " + name
    intro += ", umur saya " + age + " tahun"
    intro += ", alamat saya di " + address
    intro += ", dan saya punya hobby yaitu " + hobby + "!"

    return intro
}

var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di jalan belum jadi, dan saya punya hobby yaitu Gaming!" 