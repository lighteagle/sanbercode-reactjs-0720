// function luasLingkaran(r), Luas Lingkaran = 22/7 x jari2(r) x jari2(r)
function luasLingkaran(r) {
    return 22 / 7 * r * r
}
// function luasSegitiga(a,t), Luas Segitiga = 1/2 x alas(a) x tinggi(t)
function luasSegitiga(a, t) {
    return 1 / 2 * a * t
}
// function luasPersegi(s), Luas Persegi = sisi(s) x sisi(s)
function luasPersegi(s) {
    return s * s
}

console.log(luasLingkaran(7));
console.log(luasSegitiga(1, 2));
console.log(luasPersegi(6));

var daftarAlatTulis = ["2. Pensil", "5. Penghapus", "3. Pulpen", "4. Penggaris", "1. Buku"]

function daftar(daftarAlatTulis) {
    var arr = daftarAlatTulis.sort()
    var i = 0
    while (i < arr.length) {
        console.log(arr[i])
        i++
    }
}
daftar(daftarAlatTulis)

function tampilkanBintang(jumlahBintang) {
    var bintang = '*'
    var output = ''
    for (let i = jumlahBintang; i > 0; i--) {
        for (let j = 0; j < i; j++) {
            output += bintang
        }
        output += "\n"
    }
    return output
}
console.log(tampilkanBintang(7));

var jenisKelamin = "Lv"
var nama = "nama"

if (jenisKelamin === "L") {
    console.log("bapak " + nama)
} else if (jenisKelamin === "P") {
    console.log("ibu " + nama)
} else {
    console.log("invalid")
}