// Soal 1
console.log("-- Soal 1 --");

// const newFunction = function literal(firstName, lastName) {
//     return {
//         firstName: firstName,
//         lastName: lastName,
//         fullName: function () {
//             console.log(firstName + " " + lastName)
//             return
//         }
//     }
// }

// ES 6
const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName: () => console.log(`${firstName} ${lastName}`)
    }
}

//Driver Code 
newFunction("William", "Imoh").fullName()

// Soal 2
console.log("");
console.log("-- Soal 2 --");

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

// ES 5
// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const destination = newObject.destination;
// const occupation = newObject.occupation;

// ES 6
const {
    firstName,
    lastName,
    destination,
    occupation
} = newObject

// Driver code
console.log(firstName, lastName, destination, occupation)


// Soal 3
console.log("");
console.log("-- Soal 3 --");

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

// ES 5
// const combined = west.concat(east)

// ES 6
const combined = [...west, ...east]

//Driver Code
console.log(combined)