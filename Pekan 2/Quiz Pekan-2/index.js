// No. 11

// function filterBooksPromise(colorful, amountOfPage) {
//     return new Promise(function (resolve, reject) {
//         var books = [{
//                 name: 'sinchan',
//                 totalPage: 50,
//                 isColorful: true
//             },
//             {
//                 name: 'Kalkulus',
//                 totalPage: 250,
//                 isColorful: false
//             },
//             {
//                 name: 'doraemon',
//                 totalPage: 40,
//                 isColorful: true
//             },
//             {
//                 name: 'algoritma',
//                 totalPage: 300,
//                 isColorful: false
//             },
//         ]
//         if (amountOfPage > 0) {
//             resolve(books.filter(x => x.totalPage >= amountOfPage && x.isColorful == colorful))
//         } else {
//             var reason = new Error('maaf parameter salah')
//             reject(reason)
//         }
//     })
// }

// const testColor = false
// const testPageAmount = 1
// filterBooksPromise(testColor, testPageAmount).then(response => console.log(response)).catch(err => console.log(err))

// No. 12


// class BangunDatar {
//     constructor(name) {
//         this._name = name

//     }

//     get name() {
//         return this._name
//     }

//     get luas() {
//         return ""
//     }

//     get keliling() {
//         return ""

//     }
// }


// class Lingkaran extends BangunDatar {
//     constructor(name, jari2) {
//         super(name)
//         this.jari2 = jari2
//     }
//     get luas() {
//         return Math.PI * this.jari2 * this.jari2
//     }
//     get keliling() {
//         return 2 * Math.PI * this.jari2
//     }
// }

// class Persegi extends BangunDatar {
//     constructor(name, sisi) {
//         super(name)
//         this.sisi = sisi
//     }
//     get luas() {
//         return this.sisi * this.sisi
//     }
//     get keliling() {
//         return 4 * this.sisi
//     }

// }

// let newBD = new BangunDatar('BangunDatar')
// let newLingkaran = new Lingkaran('Linkaran', 7)
// let newPersegi = new Persegi('Persegi', 7)
// console.log(`${newBD.name} => Luas : ${newBD.luas}, Keliling : ${newBD.keliling}`)
// console.log(`${newLingkaran.name} => Luas : ${newLingkaran.luas}, Keliling : ${newLingkaran.keliling}`)
// console.log(`${newPersegi.name} => Luas : ${newPersegi.luas}, Keliling : ${newPersegi.keliling}`)


// no.13

// let pesertaLomba = [
//     ["Budi", "Pria", "172cm"],
//     ["Susi", "Wanita", "162cm"],
//     ["Lala", "Wanita", "155cm"],
//     ["Agung", "Pria", "175cm"]
// ]

// const arrToObj = arr => {
//     let newObj = []
//     arr.map(data => {
//         const nama = data[0]
//         const jenisKelamin = data[1]
//         const tinggi = data[2]
//         newObj = [
//             ...newObj,
//             {
//                 nama,
//                 jenisKelamin,
//                 tinggi
//             }
//         ]
//     })
//     return newObj
// }

// console.log(arrToObj(pesertaLomba))


// no.14
// const p = 2
// const l = 3
// const t = 1

// const s = 2

// const volumeBalok = (panjang, lebar, tinggi) => panjang * lebar * tinggi

// const volumeKubus = sisi => sisi * sisi * sisi

// console.log(`Volume : 
// Balok = ${volumeBalok(p,l,t)}
// Kubus = ${volumeKubus(s)}
// `);

// no.15
let warna = ['biru', 'merah', 'kuning', 'hijau']
let dataBukuTambahan = {
    penulis: 'jhon doe',
    tahunTerbit: 2020
}
let buku = {
    nama: 'pemrograman dasar',
    jumlahHalaman: 172,
    warnaSampul: ['hitam']
}
// gabunkanlah variabel warna(gabungkan dengan atribut warnaSampul)
// datatambahan buku ke variabel buku
// buku.warnaSampul = [...buku.warnaSampul, ...warna]

// buku = {
//     ...buku,
//     ...dataBukuTambahan
// }

// console.log(buku)