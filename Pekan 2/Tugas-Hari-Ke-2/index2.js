var readBooksPromise = require('./promise.js')

var books = [{
        name: 'LOTR',
        timeSpent: 3000
    },
    {
        name: 'Fidas',
        timeSpent: 2000
    },
    {
        name: 'Kalkulus',
        timeSpent: 4000
    }
]

let i = 0
let time = 10000


// function baca(i, time) {
// readBooks(time, books[i], sisaWaktu => {
//     if (sisaWaktu >= 0 && i < books.length - 1) {
//         i++
//         baca(i, sisaWaktu)
//     }
// })
// }

// baca(i, time)

function baca(i, time) {
    readBooksPromise(time, books[i]).then(sisaWaktu => {
        if (sisaWaktu >= 0 && i < books.length - 1) {
            i++
            baca(i, sisaWaktu)
        }
    }).catch(sisaWaktu => console.log(`perlu waktu tambahan ${-sisaWaktu} untuk bisa selesaikan buku ini`))
}

baca(i, time)