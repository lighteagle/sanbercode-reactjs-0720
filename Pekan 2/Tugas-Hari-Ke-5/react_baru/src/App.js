import React from "react";
import logo from "./logo.svg";
import "./App.css";

function App() {
  return (
    <div>
      <h1 style={{ textAlign: "center" }}>Form Pembelian Buah</h1>
      <table id="simple-board">
        <tbody>
          <tr>
            <td>
              <strong>Nama Pelanggan</strong>
            </td>
            <td>
              <input type="text" />
            </td>
          </tr>
          <tr>
            <td>
              <strong>Daftar Item</strong>
            </td>
            <td>
              <input type="checkbox" />
              Semangka <br />
              <input type="checkbox" />
              Jeruk <br />
              <input type="checkbox" />
              Nanas <br />
              <input type="checkbox" />
              Salak <br />
              <input type="checkbox" />
              Anggur <br />
            </td>
          </tr>
          <tr>
            <button type="submit"> Kirim </button>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export default App;
