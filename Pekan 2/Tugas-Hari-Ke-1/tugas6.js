// No. 1 
console.log("------------------------")
console.log("Soal No. 1")
console.log("------------------------")
// ubahlah array di bawah ini menjadi object dengan property nama, jenis kelamin, hobi dan tahun lahir
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku", 1992]

function arrToObj(arr) {

    return {
        nama: arr[0],
        "jenis kelamin": arr[1],
        hobi: arr[2],
        "tahun lahir": arr[3],
    }
}
console.log(arrToObj(arrayDaftarPeserta))

// No. 2
console.log("\n------------------------")
console.log("Soal No. 2")
console.log("------------------------")

// anda diberikan data-data buah seperti di bawah ini

// 1.nama: strawberry
//   warna: merah
//   ada bijinya: tidak
//   harga: 9000 
// 2.nama: jeruk
//   warna: oranye
//   ada bijinya: ada
//   harga: 8000
// 3.nama: Semangka
//   warna: Hijau & Merah
//   ada bijinya: ada
//   harga: 10000
// 4.nama: Pisang
//   warna: Kuning
//   ada bijinya: tidak
//   harga: 5000
// uraikan data tersebut menjadi array of object dan munculkan data pertama

var dataBuah = [{
        nama: "strawberry",
        warna: 'merah',
        'ada bijinya': 'tidak',
        harga: 9000
    },
    {
        nama: "jeruk",
        warna: 'oranye',
        'ada bijinya': 'ada',
        harga: 8000
    },
    {
        nama: "Semangka",
        warna: 'Hijau & Merah',
        'ada bijinya': 'ada',
        harga: 10000
    },
    {
        nama: "Pisang",
        warna: 'Kuning',
        'ada bijinya': 'tidak',
        harga: 5000
    }
]
console.log("array of object : \n", dataBuah)

console.log("\ndata pertama : \n", dataBuah[0])


// No. 3
console.log("\n------------------------")
console.log("Soal No. 3")
console.log("------------------------")

// soal 3
// buatlah variable seperti di bawah ini

// var dataFilm = []
// buatlah fungsi untuk menambahkan dataFilm tersebut yang itemnya object adalah object dengan property nama, durasi , genre, tahun
var dataFilm = []

function addData(arr, nama, durasi, genre, tahun) {
    arr.push({
        nama,
        durasi,
        genre,
        tahun
    })
}

addData(dataFilm, "Spiderman", "2 Jam", "Action", 2000)
addData(dataFilm, "Spiderman 2", "2 Jam", "Action", 2001)
addData(dataFilm, "Spiderman 3", "2 Jam", "Action", 2002)
console.log(dataFilm)



// No. 4
console.log("\n------------------------")
console.log("Soal No. 4")
console.log("------------------------")

// soal class
// soal 4
// Terdapat sebuah class Animal yang memiliki sebuah constructor name,
// default property band = 4 dan cold_blooded = false.

// Release 0
console.log("Release 0")

// Buatlah class Animal yang menerima satu parameter constructor berupa name.Secara
// default class Animal akan memiliki property yaitu legs(jumlah kaki) yang bernilai 4 dan cold_blooded bernilai false.

// Gunakan method getter untuk mengakses property di dalam class

class Animal {
    // Code class di sini
    constructor(name) {
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }
    get getName() {
        return this.name;
    }
    get getLegs() {
        return this.legs;
    }
    get getCold_blooded() {
        return this.cold_blooded;
    }
}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log("-Gunakan method getter untuk mengakses property di dalam class-") // "shaun"
console.log(sheep.getName) // "shaun"
console.log(sheep.getLegs) // 4
console.log(sheep.getCold_blooded) // false

// Release 1
console.log("\nRelease 1")

// Buatlah class Frog dan class Ape yang merupakan inheritance dari class Animal.Perhatikan bahwa Ape(Kera) merupakan hewan berkaki 2, hingga dia tidak menurunkan sifat jumlah kaki 4 dari class Animal.class Ape memiliki
// function yell() yang menampilkan“ Auooo” dan class Frog memiliki
// function jump() yang akan menampilkan“ hop hop”.

// Code class Ape dan class Frog di sini
class Ape extends Animal {
    constructor(name) {
        super(name)
    }
    yell() {
        return console.log("Auooo")
    }
}
class Frog extends Animal {
    constructor(name) {
        super(name)
    }

    jump() {
        return console.log("hop hop")
    }
}
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 


// No. 5
console.log("\n------------------------")
console.log("Soal No. 5")
console.log("------------------------")

// soal 5
// Terdapat sebuah class dengan nama Clock yang ditulis seperti penulisan pada function, ubahlah fungsi tersebut menjadi class dan pastikan fungsi tersebut tetap berjalan dengan baik. Jalankan fungsi di terminal/console Anda untuk melihat hasilnya. (tekan tombol Ctrl + C pada terminal untuk menghentikan method clock.start())

// Hint: Fokus soal ini hanya pada kegiatan mengubah struktur function Clock menjadi class. Jangan lupa menambahkan constructor di dalam class, dan ubah function di dalam Clock menjadi method pada class.

// function Clock({
//     template
// }) {

//     var timer;

//     function render() {
//         var date = new Date();

//         var hours = date.getHours();
//         if (hours < 10) hours = '0' + hours;

//         var mins = date.getMinutes();
//         if (mins < 10) mins = '0' + mins;

//         var secs = date.getSeconds();
//         if (secs < 10) secs = '0' + secs;

//         var output = template
//             .replace('h', hours)
//             .replace('m', mins)
//             .replace('s', secs);

//         console.log(output);
//     }

// this.stop = function () {
//     clearInterval(timer);
// };

// this.start = function () {
//     render();
//     timer = setInterval(render, 1000);
// };

// }

// var clock = new Clock({
//     template: 'h:m:s'
// });
// clock.start();

// function di atas diubah menjadi struktur class seperti berikut:

class Clock {
    constructor({
        template
    }) {
        this.template = template
    }

    render() {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);
        console.log(output);
    }
    stop() {
        clearInterval(this.timer);
    };
    start() {
        this.render()
        this.timer = setInterval(() => this.render(), 1000);
    }
}

var clock = new Clock({
    template: 'h:m:s'
});
clock.start();