// Soal 1
console.log("-- Soal 1 --");
let jari2 = 7

// luas Lingkaran
const luasLingkaran = (r) => 22 / 7 * r * r
// keliling Lingkaran
const kelilingLingkaran = (r) => 2 * 22 / 7 * r

console.log(`jari2 = ${jari2}, luas lingkaran = ${luasLingkaran(jari2)}`);
console.log(`jari2 = ${jari2}, keliling lingkaran = ${kelilingLingkaran(jari2)}`);


// Soal 2
console.log("");
console.log("-- Soal 2 --");

let kalimat = ""
const saya = 'saya'
const adalah = 'adalah'
const seorang = 'seorang'
const frontend = 'frontend'
const developer = 'developer'

kalimat = `${saya} ${adalah} ${seorang} ${frontend} ${developer} `

console.log(kalimat)


// Soal 3
console.log("");
console.log("-- Soal 3 --");

// buatlah class Book yang didalamnya terdapat property name, totalPage, price. lalu buat class baru komik yang extends terhadap buku dan mempunyai property sendiri yaitu isColorful yang isinya true atau false

class Book {
    constructor(name, totalPage, price) {
        this.name = name
        this.totalPage = totalPage
        this.price = price
    }
}

const bukuKoding = new Book('Koding', 200, 20000)

class Komik extends Book {
    constructor(name, totalPage, price, isColorful) {
        super(name, totalPage, price)
        this.isColorful = isColorful
    }
}

const komikDoraemon = new Komik('Doraemon', 150, 40000, false)

console.log(Book)
console.log(bukuKoding)
console.log(Komik)
console.log(komikDoraemon)