const form = (col, row) => {
  let output = "";

  for (let i = 0; i < row; i++) {
    for (let j = 0; j < col; j++) {
      if (j < i || j > col - i - 1) {
        output += "o";
      } else {
        output += "x";
      }
    }
    output += "\n";
  }
  return console.log(output);
};

form(9, 5);
